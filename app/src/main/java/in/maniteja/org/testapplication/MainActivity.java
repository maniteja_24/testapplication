package in.maniteja.org.testapplication;

import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import in.maniteja.org.testapplication.adapters.SlidingImage_Adapter;
import in.maniteja.org.testapplication.fragments.OneFragment;

public class MainActivity extends AppCompatActivity
{

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES= {R.drawable.bear,R.drawable.wolf,R.drawable.eagle,R.drawable.horse};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.menu));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        init();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                if(tab.getPosition() == 0)
                {
                    tabSelection(tab,R.drawable.select_video,R.color.colorPrimary);
                }
                else if(tab.getPosition() == 1)
                {
                    tabSelection(tab,R.drawable.select_image,R.color.colorPrimary);
                }
                else
                {
                    tabSelection(tab,R.drawable.select_milestone,R.color.colorPrimary);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {
                if(tab.getPosition() == 0)
                {
                    tabSelection(tab,R.drawable.video,R.color.grey);
                }
                else if(tab.getPosition() == 1)
                {
                    tabSelection(tab,R.drawable.image,R.color.grey);
                }
                else
                {
                    tabSelection(tab,R.drawable.milestone,R.color.grey);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
    }

    public void tabSelection(TabLayout.Tab tab,int drawableId,int colorId)
    {
        tab.setIcon(drawableId);
        View v = tab.getCustomView();
        ImageView image = (ImageView) v.findViewById(R.id.tabIcon);
        TextView text = (TextView) tab.getCustomView().findViewById(R.id.tab);
        image.setBackgroundResource(drawableId);
        text.setTextColor(getResources().getColor(colorId));
        tab.setCustomView(v);
        viewPager.setCurrentItem(tab.getPosition());
    }

    private void init() {


        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(MainActivity.this,ImagesArray));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(5 * density);



        NUM_PAGES =IMAGES.length;



        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private void setupTabIcons() {
        addTab(0,"VIDEOS",R.drawable.select_video,R.color.colorPrimary);
        addTab(1,"IMAGES",R.drawable.image,R.color.grey);
        addTab(2,"MILESTONE",R.drawable.milestone,R.color.grey);
    }

    public void addTab(int position,String text,int drawableId,int colorId)
    {
        View root1 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tabOne = (TextView) root1.findViewById(R.id.tab);
        tabOne.setText(text);
        tabOne.setTextColor(getResources().getColor(colorId));
        ImageView tabIcon1 = (ImageView) root1.findViewById(R.id.tabIcon);
        tabIcon1.setBackgroundResource(drawableId);
        tabLayout.getTabAt(position).setCustomView(root1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OneFragment(), "");
        adapter.addFrag(new OneFragment(), "");
        adapter.addFrag(new OneFragment(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    OneFragment frag = new OneFragment();
                    return frag;
                case 1:
                    OneFragment frag1 = new OneFragment();
                    return frag1;
                case 2:
                    OneFragment frag2 = new OneFragment();
                    return frag2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
