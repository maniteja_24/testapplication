package in.maniteja.org.testapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.maniteja.org.testapplication.R;
import in.maniteja.org.testapplication.models.RowModel;

/**
 * Created by maniteja on 7/19/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>
{
    private List<RowModel> moviesList;
    private Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title, subImageTitle,text;
        public ImageView image,subImage;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subImageTitle = (TextView) view.findViewById(R.id.subImageTitle);
            text = (TextView) view.findViewById(R.id.text);
            image = (ImageView) view.findViewById(R.id.mainImage);
            subImage = (ImageView) view.findViewById(R.id.subImage);
        }
    }

    public RecyclerAdapter(Context ctx,List<RowModel> moviesList) {
        this.ctx = ctx;
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerviewrow, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RowModel movie = moviesList.get(position);

        holder.title.setText(movie.getTitle());
        holder.subImageTitle.setText(movie.getSubImageTitle());
        holder.text.setText(movie.getText());
        holder.image.setBackgroundResource(movie.getMainImage());
        holder.subImage.setBackgroundResource(movie.getSubImage());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
