package in.maniteja.org.testapplication.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;

import in.maniteja.org.testapplication.R;
import in.maniteja.org.testapplication.adapters.RecyclerAdapter;
import in.maniteja.org.testapplication.customviews.SimpleDividerItemDecoration;
import in.maniteja.org.testapplication.models.RowModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment
{

    RecyclerView recyclerView;
    RecyclerAdapter mAdapter;

    ArrayList<RowModel> agp;

    public OneFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_one, container, false);

        agp = new ArrayList<RowModel>();

        try{
            new LoadData().execute();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        recyclerView = (RecyclerView) rootView.findViewById(R.id.historyData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    private class LoadData extends AsyncTask<Void, Void, String>
    {
        private ProgressDialog dialog = new ProgressDialog(getActivity());

        LoadData() throws IOException
        {
            dialog.setCancelable(false);
            dialog.setMessage(getResources().getString(R.string.pleasewaitprogress));
            dialog.show();
        }

        @Override
        protected void onPreExecute()
        {
            // TODO Auto-generated method stub

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(Void... params)
        {

            agp.add(new RowModel(R.drawable.bear,"EMPTINESS FT.JUSTIN TIBLEKAR",R.drawable.bear,"18 HOURS AGO","Lorem lpsum is simply dummy text of the printing and type setting industry."));
            agp.add(new RowModel(R.drawable.eagle,"I'M FALLING LOVE WITH YOU",R.drawable.eagle,"20 HOURS AGO","Lorem lpsum is simply dummy text of the printing and type setting industry."));
            agp.add(new RowModel(R.drawable.owl,"BABY FT.JUSTIN BABER",R.drawable.owl,"22 HOURS AGO","Lorem lpsum is simply dummy text of the printing and type setting industry."));
            return "";
        }

        @Override
        protected void onPostExecute(String result)
        {

            super.onPostExecute(result);

            mAdapter = new RecyclerAdapter(getContext(),agp);
            recyclerView.setAdapter(mAdapter);

            dialog.dismiss();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

    }
}
