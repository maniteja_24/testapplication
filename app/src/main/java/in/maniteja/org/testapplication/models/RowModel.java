package in.maniteja.org.testapplication.models;

/**
 * Created by maniteja on 7/19/17.
 */

public class RowModel
{
    int mainImage;
    String title;
    int subImage;
    String subImageTitle;
    String text;

    public RowModel()
    {
    }

    public RowModel(int mainImage, String title, int subImage, String subImageTitle, String text)
    {
        this.mainImage = mainImage;
        this.title = title;
        this.subImage = subImage;
        this.subImageTitle = subImageTitle;
        this.text = text;
    }

    public int getMainImage()
    {
        return mainImage;
    }

    public RowModel setMainImage(int mainImage)
    {
        this.mainImage = mainImage;
        return this;
    }

    public String getTitle()
    {
        return title;
    }

    public RowModel setTitle(String title)
    {
        this.title = title;
        return this;
    }

    public int getSubImage()
    {
        return subImage;
    }

    public RowModel setSubImage(int subImage)
    {
        this.subImage = subImage;
        return this;
    }

    public String getSubImageTitle()
    {
        return subImageTitle;
    }

    public RowModel setSubImageTitle(String subImageTitle)
    {
        this.subImageTitle = subImageTitle;
        return this;
    }

    public String getText()
    {
        return text;
    }

    public RowModel setText(String text)
    {
        this.text = text;
        return this;
    }
}
